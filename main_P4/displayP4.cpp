#include <Arduino.h>
#include "displayP4.h"
#include "frases.h"

byte row;
int currentNumber = 5;
int color;
byte* matrixShow;
bool doble = false;

void initMatrix()
{
  pinMode(A,OUTPUT);
  pinMode(B,OUTPUT);
  pinMode(C,OUTPUT);
  pinMode(D,OUTPUT);
  pinMode(OE,OUTPUT);
  pinMode(CLK,OUTPUT);
  pinMode(SCLK,OUTPUT);
  pinMode(R_data,OUTPUT);
  pinMode(G_data,OUTPUT);
  pinMode(B_data,OUTPUT);

  digitalWrite(OE, LOW);
  digitalWrite(A, LOW);
  digitalWrite(B, LOW);
  digitalWrite(C, LOW);
  digitalWrite(D, LOW);
  digitalWrite(SCLK, LOW);
  digitalWrite(CLK, LOW);
  digitalWrite(R_data,LOW);
  digitalWrite(G_data,LOW);
  digitalWrite(B_data,LOW);
}

void showMatrix(int value)
{
    if (value != currentNumber)
    {
      matrixShow = selectMatrix(value);
      currentNumber = value;
    }
    digitalWrite(OE, LOW);
    selectRow();

    for(int section=0; section<=390; section+=10) // Selecciona la sección de lectura
    {
      byte sectionValue = matrixShow[row + section];
      for(int bit=0; bit <= 7; bit++) // Selecciona el bit de lectura
      {
        digitalWrite(color,  (sectionValue >> bit) & 0x01);
        if (doble) digitalWrite(R_data,  (sectionValue >> bit) & 0x01);
        digitalWrite(CLK, HIGH);
        digitalWrite(CLK, LOW);
      }
    }
    digitalWrite(SCLK, HIGH);
    digitalWrite(SCLK, LOW);
    
    delayMicroseconds(1000);
    digitalWrite(OE, HIGH);
    if (row == 9) row =-1;
    row++;
}

// Selecciona la matriz correspondiente al numero
byte* selectMatrix(byte number)
{
  switch (number)
  {
    case 1:
      color = G_data;
      doble = false;
      return siga;
    case 2:
      color = R_data;
      doble = false;
      return espere;
    case 3:
      color = G_data;
      doble = true;
      return saliendo;
  }
}

void selectRow()
{
  switch (row)
  {
    case 0:
      digitalWrite(A, LOW);
      digitalWrite(B, LOW);
      digitalWrite(C, LOW);
      digitalWrite(D, LOW);
      break;
    case 1:
      digitalWrite(A, HIGH);
      digitalWrite(B, LOW);
      digitalWrite(C, LOW);
      digitalWrite(D, HIGH);
      break;
    case 2:
      digitalWrite(A, LOW);
      digitalWrite(B, LOW);
      digitalWrite(C, LOW);
      digitalWrite(D, HIGH);
      break;
    case 3:
      digitalWrite(A, HIGH);
      digitalWrite(B, HIGH);
      digitalWrite(C, HIGH);
      digitalWrite(D, LOW);
      break;
    case 4:
      digitalWrite(A, LOW);
      digitalWrite(B, HIGH);
      digitalWrite(C, HIGH);
      digitalWrite(D, LOW);
      break;
    case 5:
      digitalWrite(A, HIGH);
      digitalWrite(B, LOW);
      digitalWrite(C, HIGH);
      digitalWrite(D, LOW);
      break;
    case 6:
      digitalWrite(A, LOW);
      digitalWrite(B, LOW);
      digitalWrite(C, HIGH);
      digitalWrite(D, LOW);
      break;
    case 7:
      digitalWrite(A, HIGH);
      digitalWrite(B, HIGH);
      digitalWrite(C, LOW);
      digitalWrite(D, LOW);
      break;
    case 8:
      digitalWrite(A, LOW);
      digitalWrite(B, HIGH);
      digitalWrite(C, LOW);
      digitalWrite(D, LOW);
      break;
    case 9:
      digitalWrite(A, HIGH);
      digitalWrite(B, LOW);
      digitalWrite(C, LOW);
      digitalWrite(D, LOW);
      break;
  }
}