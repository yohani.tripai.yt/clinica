#include <Arduino.h>
#include "wifi.h"
#include "displayP4.h"


const char* ssid = "ESP_Indicador";
const char* password = "Ind.2024:YT";

const char* URL = "http://192.168.5.1/ESP32info";

const int requestInterval = 7000;


void initWifi()
{
  WiFi.begin(ssid, password);
  while(WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("conectado");
}

void bucleWifi(String& answer)
{
  // && millis() - tiempo > umbral
  if(WiFi.status() == WL_CONNECTED)
  {
    answer = getRequest(URL);
  }
}

String getRequest(const char* urlName)
{
  HTTPClient http;
  http.begin(urlName);
  // Enviar peticiones HTTP
  int httpResponseCode = http.GET();

  String payload = "...";
  
  if (httpResponseCode > 0 )
  {
    payload = http.getString();
  }
  http.end();
  return payload;
}