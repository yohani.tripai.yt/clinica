#ifndef DISPLAYP4_H
#define DISPLAYP4_H
#include <Arduino.h>

// #define A 5
// #define B 6
// #define C 7
// #define D 8
// #define OE 41
// #define CLK 12 
// #define SCLK 10
// #define R_data 11
// #define G_data 4

#define A 8
#define B 10
#define C 6
#define D 11  //
#define OE 13
#define CLK 14 //
#define SCLK 12
#define R_data 4 //4
#define G_data 9 //9
#define B_data 5 //

void initMatrix();
void showMatrix(int value);
byte* selectMatrix(byte number);
void selectRow();

#endif