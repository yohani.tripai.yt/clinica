#ifndef WIFI_H
#define WIFI_H

#include <Arduino.h>
#include <WiFi.h>
#include <HTTPClient.h>

void initWifi();
void bucleWifi(String& answer);
String getRequest(const char* urlName);


#endif