#ifndef FUNCIONES_H
#define FUNCIONES_H
#include <Arduino.h>
#include <WiFiClient.h>

/************** EEPROM **************/

class AlmacenamientoEEPROM {

  public:
      AlmacenamientoEEPROM(int ByteForData, int ByteForStoreAddress, bool show);
      void getData(byte& dataOut);
      void pushData(byte newdata);
      void showme();

  private:
      int cntByteForData; // máximo de 255
      int storeAddress;  // máximo de 255
      int size_EEPROM;  // máximo 4096
      bool packageActive;
      bool _show;
      byte addressOfAddress;
      byte cntUsed;
      byte address;
      byte actualDate;
      byte newData;

      void newPackage();
};

/************** WiFi **************/

void iniciarWiFi(AlmacenamientoEEPROM& PROM, byte* value, bool show);
void bucle(byte* value, byte* state);
void handleConnectionRoot();
void handleDevice1();
void aumentar();
void disminuir();
void reset();
void guardarNumero();
String mensaje();
String estado();


/************** Lógica de sensores **************/

#define sensorInicio 1
#define sensorFinal 5
#define sensorPorton 2


class Sensor {
public:
  Sensor(int pin);
  void proceso();
  bool estado;
  bool activado;

private:
  int pin;
  int estadoActual; // privado
  int estadoAnterior; // privado
  const unsigned long tiempoUmbralDeteccionSensor = 3000; // PRIVADO
  const unsigned long tiempoCambioMensaje = 1000;
  unsigned long tiempoInicio;
  unsigned long tiempoFinal;
  
};

void initSensor();
void logica(byte& state, byte& value);
#endif