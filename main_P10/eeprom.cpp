#include <Arduino.h>
#include <EEPROM.h>
#include "funciones.h"



/* AlmacenamientoEEPROM
Args:
int ByteForData         -- Cantidad de byte que se utlizan para intercalar el número de estacionamientos disponibles.
int ByteForStoreAddress -- Cantidad de paquetes para almacenar la información del uso de los byte que almacenan la cantidad de espacios disponibles.
bool _show              -- Valor para enseñar o no información en el monitor serial.
*/
AlmacenamientoEEPROM::AlmacenamientoEEPROM(int ByteForData, int ByteForStoreAddress, bool show) : cntByteForData(ByteForData), storeAddress(ByteForStoreAddress), _show(show)
{
  // Valor para inicializar la memoria EEPROM,
  // Suma los byte's de almacenamientos con los 3 byte por paquete a utilizar
  size_EEPROM = cntByteForData + (storeAddress*3);
}

/* getData
Args:
byte& dataOut   -- Referencia de la variable que contiene la cantidad de estacionemientos.

Hace un barrido buscando el paquete de almacenamiento de direcciones que tiene almacenada la direción del byte que se está utilizando actualmente.
Se recomienda su uso sólo al principio ya que posee un alto coste computacinal.
*/
void AlmacenamientoEEPROM::getData(byte& dataOut)
{
  EEPROM.begin(size_EEPROM+1);
  for (int i = cntByteForData ; i < size_EEPROM; i = i + 3)
  {
    EEPROM.get(i, packageActive);
    if (packageActive == true)
    {
      addressOfAddress = i;
      EEPROM.get(i+1, cntUsed);
      EEPROM.get(i+2, address);
      EEPROM.get(address, actualDate);
      dataOut = actualDate;
      if (_show)
      {
        Serial.println("---------------------------- Datos Iniciales ----------------------------");
        Serial.print("Dato Actual: ");
        Serial.print(actualDate);
        Serial.print(" en la dirección: ");
        Serial.print(address);
        Serial.print(" Se ha usado este paquete de direccion : ");
        Serial.print(cntUsed);
        Serial.print(" veces");
        Serial.print("  Datos de dirección almacenado en: ");
        Serial.println(addressOfAddress);
        Serial.println("---------------------------------- END ----------------------------------");
      }
      EEPROM.end();
      break;
    }
  }
}



/*pushData
Args:
byte receivedData -- Dato nuevo para ser almacenado

Almacena el nuevo dato en un byte de la EEPROM y almacena la dirección de éste dato en alguno de los paquetes de dirección.
*/
void AlmacenamientoEEPROM::pushData(byte receivedData)
{
  newData = receivedData;
  EEPROM.begin(size_EEPROM+1);

  EEPROM.get(addressOfAddress+2,address);
  byte oldAddress = address;
  address ++;
  EEPROM.get(addressOfAddress + 1, cntUsed);
  if (address >= cntByteForData)
  {
    address = 0;
    newPackage();
  }
  
  EEPROM.put(address, newData);
  EEPROM.commit();
  EEPROM.put(addressOfAddress+2, address);
  EEPROM.commit();

  EEPROM.end();
  if (_show)
  {
    Serial.println("- - - - - - - - - - - - - - - - - NEW - - - - - - - - - - - - - - - - -");
    Serial.println("---------------------------- Datos Grabados ----------------------------");
    Serial.print("Nuevo dato: ");
    Serial.print(newData);
    Serial.print(" almacenado en: ");
    Serial.print(address);
    Serial.print(" Direccion grabada en: ");
    Serial.println(addressOfAddress+2);
  }
}

/* newPackage
Cambia el paquete que se estaba actualizando al siguiente.
*/
void AlmacenamientoEEPROM::newPackage()
{
  EEPROM.put(addressOfAddress, false);
  EEPROM.commit();
  EEPROM.get(addressOfAddress + 1, cntUsed);
  cntUsed ++;
  EEPROM.commit();
  delay(5);
  EEPROM.put(addressOfAddress + 1, cntUsed);
  EEPROM.commit();
  if (_show)
  {
    Serial.println("---------------- Cambiando de paquete de alamacenamiento -------------------");
    Serial.print("Direccion cambiada de : ");
    Serial.print(addressOfAddress);
    Serial.print(" fue usada: ");
    Serial.print(cntUsed);
    Serial.println(" veces ");
  }

  addressOfAddress = addressOfAddress + 3;

  if (addressOfAddress >= size_EEPROM)
  {
    addressOfAddress = cntByteForData;
  }
  EEPROM.put(addressOfAddress , true);
  EEPROM.commit();
  EEPROM.get(addressOfAddress + 1, cntUsed);
  EEPROM.commit();
  EEPROM.put(addressOfAddress + 2, address);
  EEPROM.commit();
  if (_show)
  {
    Serial.print(" Dirección nueva de almacenamiento: ");
    Serial.print(addressOfAddress);
    Serial.print(" uso de esta nueva dirección: ");
    Serial.println(cntUsed);
  }
}

/* show
Enseña los datos almacenados en la EEPROM de forma ordenada
*/
void AlmacenamientoEEPROM::showme()
{
  Serial.println("-------------------- Datos en EEPROM --------------------");
  EEPROM.begin(size_EEPROM+1);
  byte value;

  byte paqueteActivacion;
  byte cantidadUso;
  byte dir;
  for (int i = 0; i < cntByteForData; i++)
  {
    EEPROM.get(i, value);
    Serial.print("Direccion: ");
    Serial.print(i);
    Serial.print("  Dato almacenado: ");
    Serial.println(value);
  }
  int n = 0;
  for (int i = cntByteForData; i < size_EEPROM; i = i + 3)
  {
    Serial.print("- - -  ");
    Serial.print(n);
    Serial.println("  - - -");
    EEPROM.get(i, paqueteActivacion);
    EEPROM.get(i+1, cantidadUso);
    EEPROM.get(i+2, dir);

    Serial.print("Direccion: ");
    Serial.print(i);
    Serial.print("  Dato de paquete activación: ");
    Serial.println(paqueteActivacion);

    Serial.print("Direccion: ");
    Serial.print(i+1);
    Serial.print("  Dato de la cantidad de uso del paquete: ");
    Serial.println(cantidadUso);

    Serial.print("Direccion: ");
    Serial.print(i+2);
    Serial.print("  Dato de la direccion almacenada: ");
    Serial.println(dir);
    n++;
  }
  for (int i = cntByteForData; i <= size_EEPROM; i++)
  {

  }
  Serial.println("- - - - - - - - - - - - - END - - - - - - - - - - - - - - -");
  EEPROM.end();
}
