#include <Arduino.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>

#include "funciones.h"

AlmacenamientoEEPROM* _prom = nullptr;
byte* _value;
byte* _state;
bool _show;

const char* ssid = "ESP_Indicador";
const char* password = "Ind.2024:YT";

IPAddress local_ip(192, 168, 5, 1);
IPAddress gateway(192, 168, 5, 1);
IPAddress subnet(255, 255, 255, 0);

// WiFiServer server(80);
WebServer server(80);

/* ServidorWeb
Args: 
const char* ssid            -- ID de la conexión wifi
const char* password        -- Contraseña para la conexión wifi
AlmacenamientoEEPROM& PROM  -- Clase para trabajar con la memoria EEPROM
byte& value                 -- Valor exterior de la cantidad de estacionamientos disponibles

Realiza toda la lógica de la Pagina Web para lograr enseñar la cantidad de espacios disponibles y gestionar solicitudes de aumentar o disminuir dicho
número, además, de volverlo a cero o proporcionar un número cualquiera dentro de un rango determinado
*/
/* iniciarWifi
Inicia el servidor Web, mediante los datos porporcionados
*/
void iniciarWiFi(AlmacenamientoEEPROM& PROM, byte* value, bool show) {
  _prom = &PROM;
  _value = value;
  _show = show;

  WiFi.softAP(ssid, password);
  WiFi.softAPConfig(local_ip, gateway, subnet);
  delay(100);
  server.on("/", handleConnectionRoot);
  server.on("/ESP32info", handleDevice1);
  server.begin();
}

void handleConnectionRoot() {
  if (server.args() > 0) {
    // Manejar parámetros enviados en la solicitud
    String action = server.argName(0);
    if (action == "aumentar") {
      aumentar();
    } else if (action == "disminuir") {
      disminuir();
    } else if (action == "reset") {
      reset();
    } else if (action == "guardarNumero") {
      guardarNumero();
    }
  }

  server.send(200, "text/html", mensaje());
}

void handleDevice1() 
{
  if (*_state == 1)
  {
    server.send(200, "text/html", "SIGA");
  }
  else if (*_state == 2)
  {
    server.send(200, "text/html", "ESPERE");
  }
  else if (*_state == 3)
  {
    server.send(200, "text/html", "SALIENDO");
  }
  else
  {
    server.send(200, "text/html", "SIGA");
  }
}


void bucle(byte* value, byte* state) 
{
  server.handleClient();
  _value = value;
  _state = state;
}



/* principal
Almacena y refresca la información para enseñar en la página, formato HTML.
Retuns:
String html -- Código para enseñar el front de la página web.
*/
String mensaje() {
  String html = "<html><head><style>";
  html += "body { text-align: center; }";
  html += "h1 { color: #333; }";
  html += "button { font-size: 25px; padding: 15px 25px; margin: 15px; }";
  html += ".button-container { display: flex; justify-content: center; }";
  html += "input[type='number'] {width: 250px; font-size: 25px}";
  html += "input[type='submit'] {width: 150px; font-size: 25px}";
  html += "</style></head><body>";

  html += "<h1>Cantidad de estacionamientos disponibles</h1>";
  html += "<h1>Casa de Consulta</h1>";
  // html += "<h2>: </h2>";
  html += "<h1 style='font-size: 60px;'>" + String(*_value) + "<\h1>";

  // Contenedor para los botones de aumentar y disminuir en la misma línea
  html += "<div class='button-container'>";
  html += "<button onclick='aumentar()'>Aumentar</button>";
  html += "<button onclick='disminuir()'>Disminuir</button>";
  html += "</div>";

  html += "<div class='button-container'>";
  html += "<button onclick='reset()'>Reset</button>";
  html += "</div>";

  // Formulario para ingresar el número de estacionamientos
  html += "<form action='/?guardarNumero=1' method='post'>";
  html += "<label for='numero'>Modificar valor:</label><br />";
  html += "<input type='number' id='numero' name='numero' min='0' max='100' placeholder='Ingrese el numero' />";
  html += "<input type='submit' value='Guardar' />";
  html += "</form>";

  // JavaScript para las funciones de aumentar y disminuir
  html += "<script>";
  html += "function aumentar() { window.location.href = '/?aumentar=1'; }";
  html += "function disminuir() { window.location.href = '/?disminuir=1'; }";
  html += "function reset() { window.location.href = '/?reset=1'; }";
  html += "</script>";

  html += "</body></html>";
  return html;
}

String estado() {
  return "ingresando vehículo";
}

/* aumentar
Aumenta la cantidad de espacios diponibles si se presiona el botón.
Almacena dicho valor en la memoria EEPROM.
Cambia el valor exterior por el número proporcionado por la página.
*/
void aumentar() {
  (*_value)++;
  if (*_value >= 100) {
    *_value = 99;
  }
  _prom->pushData(*_value);
  if (_show) {
    _prom->showme();
  }
}

/* disminuir
Disminuye la cantidad de espacios diponibles si se presiona el botón.
Almacena dicho valor en la memoria EEPROM.
Cambia el valor exterior por el número proporcionado por la página.
*/
void disminuir() {

  if (*_value <= 0) {
    *_value = 0;
  } else {
    (*_value)--;
  }
  _prom->pushData(*_value);
  if (_show) {
    _prom->showme();
  }
}

/* reset
Coloca en cero la cantidad de espacios diponibles si se presiona el botón.
Almacena dicho valor en la memoria EEPROM.
Cambia el valor exterior por el número proporcionado por la página.
*/
void reset() {
  *_value = 0;
  _prom->pushData(*_value);
  if (_show) {
    _prom->showme();
  }
}

/* guardarNumero
Obtiene el número ingresado por el usuario, lo transforma a entero.
Almacena dicho valor en la memoria EEPROM.
Cambia el valor exterior por el número proporcionado por la página.
*/
void guardarNumero() {
  // Obtener el valor del cuadro de entrada de número
  String inputNumero = server.arg("numero");

  // Convertir el valor a un entero y asignarlo a la variable del contador
  *_value = inputNumero.toInt();
  _prom->pushData(*_value);
}

