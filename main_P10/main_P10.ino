#include "funciones.h"
#include "displayP10.h"


/* PÁRAMETROS DEL CONTEO DE ESPACIOS DISPONIBLES */
byte value;
byte valueAnterior;
byte state;

/* CREACIÓN DE LOS OBEJTOS */
AlmacenamientoEEPROM prom(10,10, false);


void setup() 
{
  Serial.begin(115200);
  prom.getData(value);
  valueAnterior = value;
  iniciarWiFi(prom, &value, false);
  initMatrix();
}

void loop() 
{
  
  logica(state, value);
  
  bucle(&value, &state);
  showMatrix(value);
  if (value != valueAnterior)
  {
    valueAnterior = value;
    prom.pushData(value);
  }

}

