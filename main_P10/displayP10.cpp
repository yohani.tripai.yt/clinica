#include <Arduino.h>
#include "displayP10.h"
#include "numeros.h"

byte row;
byte valueBit;
byte previousNumber;
byte currentNumber;

byte ten;
byte unit;


byte matrixNumber[64];
// byte matrixTen[32];
// byte matrixUnit[32];

void initMatrix()
{
  pinMode(A,OUTPUT);
  pinMode(B,OUTPUT);
  pinMode(OE,OUTPUT);
  pinMode(CLK,OUTPUT);
  pinMode(SCLK,OUTPUT);
  pinMode(R_data,OUTPUT);

  digitalWrite(OE, LOW);
  digitalWrite(A, LOW);
  digitalWrite(B, LOW);
  digitalWrite(SCLK, LOW);
  digitalWrite(CLK, LOW);
  digitalWrite(R_data,HIGH);
}

void showMatrix(byte value)
{
  if (value != currentNumber)
  {
    completeMatrix(value);
  }
  if (value == 0)
  {
    completeMatrix(0);
  }
  digitalWrite(R_data,HIGH);
  digitalWrite(CLK, HIGH);
  digitalWrite(SCLK, HIGH);

  selectRow();
  for(int section=0; section<=60; section+=4) // Selecciona la sección de lectura
  {
    for(int bit=7; bit>=0; bit--) // Selecciona el bit de lectura
    {
      valueBit = bitRead(matrixNumber[row+section], bit);
      digitalWrite(R_data, !valueBit);
      digitalWrite(CLK, LOW);
      digitalWrite(CLK, HIGH);
    }
  }
  digitalWrite(SCLK, LOW);
  digitalWrite(SCLK, HIGH);
  delay(2);
  if (row == 3) row =-1;
  row++;

  digitalWrite(OE, HIGH);
  delayMicroseconds(brillo);
  digitalWrite(OE, LOW);
  delayMicroseconds(500);
}

// Genera matriz del número completo
void completeMatrix(byte value)
{
  ten = value / 10;
  unit = value % 10;
  byte* matrixTen = selectMatrix(ten);
  byte* matrixUnit = selectMatrix(unit);
  memcpy(matrixNumber, matrixTen, 32);
  memcpy(matrixNumber + 32, matrixUnit, 32);
}


// Selecciona la matriz correspondiente al numero
byte* selectMatrix(byte number)
{
  switch (number)
  {
    case 0:
      return cero;
    case 1:
      return uno;
    case 2:
      return dos;
    case 3:
      return tres;
    case 4:
      return cuatro;
    case 5:
      return cinco;
    case 6:
      return seis;
    case 7:
      return siete;
    case 8:
      return ocho;
    case 9:
      return nueve;
    default:
      return cero;
  }
}

void selectRow()
{
  switch (row)
  {
    case 0:
      digitalWrite(A, HIGH);
      digitalWrite(B, HIGH);
      break;
    case 1:
      digitalWrite(A, LOW);
      digitalWrite(B, HIGH);
      break;
    case 2:
      digitalWrite(A, HIGH);
      digitalWrite(B, LOW);
      break;
    case 3:
      digitalWrite(A, LOW);
      digitalWrite(B, LOW);
      break;
  }
}