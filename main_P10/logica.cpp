#include <Arduino.h>

#include "funciones.h"

bool estadoPorton = 0; // 0: cerrado; 1: abierto

const unsigned long tiempoUmbralParaEntrarSalir = 15000;
unsigned long tiempo = 0;

bool estadoPrevio = false;
int continuar = 0;

/**********************************/
enum Estado
{
  ESPERAR_SENSOR,
  ENTRANDO,
  SALIENDO
};
/**********************************/
Sensor entrada(sensorInicio);
Sensor salida(sensorFinal);

Estado estadoActual = ESPERAR_SENSOR;


void initSensor()
{
  pinMode(sensorInicio, INPUT);
  pinMode(sensorFinal, INPUT);
  pinMode(sensorPorton, INPUT);
}

void logica(byte& state, byte& value)
{
  entrada.proceso();
  salida.proceso();
  switch (estadoActual)
  {
    case(ESPERAR_SENSOR):
      if(entrada.activado)
      {
        state = 2;
      }
      else
      {
        state = 1;
      }
      
      if(entrada.estado)
      {
        Serial.println("esperando sensores");
        Serial.print("primera entrada");
        value--;
        continuar++;
        tiempo = millis();
        Serial.println("\t\t valor " + String(value) + "  continuar " + String(continuar));
        estadoActual = ENTRANDO;
      }
      else if (salida.estado)
      {
        Serial.println("esperando sensores");
        Serial.print("primera salida");
        value++;
        continuar++;
        tiempo = millis();
        Serial.println("\t\t valor " + String(value) + "  continuar " + String(continuar));
        estadoActual = SALIENDO;
      }
      break;
    /************************************/
    case(ENTRANDO):
      
      state = 2;
      if(salida.estado)
      {
        continuar--;
        Serial.println("confirmado una entrada");
      }
      if(entrada.estado)
      {
        continuar++;
        value--;
        Serial.print("ENTRANDO  ");
        Serial.println("\t\t value " + String(value)+  "  continuar  " + String(continuar));
        tiempo = millis();
      }
      else if(continuar == 0 || millis() - tiempo > tiempoUmbralParaEntrarSalir)
      {
        value = value + continuar;
        continuar = 0;
        Serial.print("ESPERAR SESNORES ");
        Serial.println("\t\t value " + String(value)+  "  continuar  " + String(continuar));
        estadoActual = ESPERAR_SENSOR;
      }
      break;
    /***********************************/
    case(SALIENDO):
      
      state = 3;
      if(entrada.estado)
      {
        continuar--;
        Serial.println("confirmado una salida");
      }
      if(salida.estado)
      {
        continuar++;
        value++;
        Serial.print("SALIENDO");
        Serial.println("\t\t\t value " + String(value)+  "  continuar  " + String(continuar));
        tiempo = millis();
      }
      else if(continuar == 0 || millis() - tiempo > tiempoUmbralParaEntrarSalir)
      {
        value = value - continuar;
        continuar = 0;
        Serial.print("ESPERAND SENSORES");
        Serial.println("\t\t value " + String(value)+  "  continuar  " + String(continuar));
        estadoActual = ESPERAR_SENSOR;
      }
      break;
  }
}

/***************** CLASE PARA LECTURA DE LOS SENSORES *****************/
Sensor::Sensor(int pin) : pin(pin)
{
  int estadoActual = HIGH; // privado
  int estadoAnterior = HIGH; // privado
   // PRIVADO
   //Priv
  unsigned long tiempoInicio = 0;
  unsigned long tiempoFinal = 0;

  bool estado = false; // publico
  bool activado = false; // publico

  pinMode(pin, INPUT);
}

void Sensor::proceso()
{
  estadoActual = digitalRead(pin);

  if (estadoActual == estadoAnterior)
  {
    estado = false;
  }
  else if (estadoActual != estadoAnterior)
  { //vehiculo entrando
    if (estadoActual == LOW)
    {
      //flanco de bajada
      tiempoInicio = millis();
    }else
    {
      //flanco de subida
      tiempoFinal = millis();
      if (tiempoFinal - tiempoInicio > tiempoUmbralDeteccionSensor)
      {
        estado = true;
      }
    }
    estadoAnterior = estadoActual;
  }
  if (estadoActual == LOW && millis() - tiempoInicio > tiempoCambioMensaje)
  {
    activado = true;
  }else
  {
    activado = false;
  }

  // if (millis() - tiempoFinal > 100)
  // {
  //   estado = false;
  // }
}
