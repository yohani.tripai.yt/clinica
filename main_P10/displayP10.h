#ifndef DISPLAYP10_H
#define DISPLAYP10_H
#include <Arduino.h>

#define A 37
#define B 36
#define OE 4
#define CLK 35
#define SCLK 47
#define R_data 21
#define brillo 500


// #define A 36 //
// #define B 35 //
// #define OE 12 //
// #define CLK 37 //
// #define SCLK 47 ///
// #define R_data 21 //

void initMatrix();
void showMatrix(byte value);
void completeMatrix(byte value);
byte* selectMatrix(byte number);
void selectRow();

#endif