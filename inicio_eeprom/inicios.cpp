#include <EEPROM.h>
#include "Arduino.h"
byte contador = 0;
byte dato;
byte n = 20;
byte numero = 255;


void inicio_1()
{
  Serial.begin(115200);
  delay(1000);
  randomSeed(analogRead(0));

  Serial.println();
  Serial.println("start");
  EEPROM.begin(n);
  Serial.print("largo de la EEPROM: ");
  Serial.println(EEPROM.length());

  for (int i = 0; i < 10 ; i++)
  {
    EEPROM.put(i, random(0, 101));
    EEPROM.commit();
  }
  EEPROM.put(10, 9);
  EEPROM.commit();
  for (int j = 11; j < 19; j++)
  {
    EEPROM.put(j, contador);
    contador++;
    EEPROM.commit();
  }
  EEPROM.put(19, numero);
  EEPROM.commit();
  for (int i = 0; i < EEPROM.length(); i++)
  {
    EEPROM.get(i, dato);
    Serial.print("dato ");
    Serial.print(i);
    Serial.print(" : ");
    Serial.println(dato);
  }

  EEPROM.end();
  Serial.println("end");
}

void inicio_2()
{
  Serial.begin(115200);
  delay(1000);
  EEPROM.begin(21);
  for (int i = 0; i < EEPROM.length(); i++)
  {
    EEPROM.get(i, dato);
    Serial.print("dato ");
    Serial.print(i);
    Serial.print(" : ");
    Serial.println(dato);
  }
  EEPROM.end();
}


int cntByteForData = 10; // máximo de 255
int storeAddress = 10;  // máximo de 255
int size_EEPROM = (cntByteForData + (storeAddress*3)) ; 

void inicio_3()
{
  EEPROM.begin(size_EEPROM+1);
  Serial.println();
  Serial.print("tamaño de EEPROM: ");
  Serial.println(EEPROM.length());
  // Inicio de los byte de almacenamientos en cero
  for (int i= 0; i <= cntByteForData; i++)
  {
    EEPROM.put(i, 2);
    EEPROM.commit();
  }
  
  for (int i = cntByteForData; i < size_EEPROM; i = i + 3)
  {
    // Inicio de los byte de datos, en bajo todos los activadores de paquete
    EEPROM.put(i, false);
    EEPROM.commit();
    Serial.println(i);
    // Inicio del byte de cantidad de uso en cero
    EEPROM.put(i+1 , 1);
    EEPROM.commit();
    Serial.println(i+1);
    // Inicio de las direcciones, en cero todas
    EEPROM.put(i+2 , 0);
    EEPROM.commit();
    Serial.println(i+2);
  }
  // Activa el primer paquete en alto
  EEPROM.put(cntByteForData, true);
  EEPROM.commit();
  EEPROM.end();
}

void show(int size)
{
  Serial.println("-------------------- Datos en EEPROM --------------------");
  EEPROM.begin(size);
  byte value;
  for (int i = 0; i <= size; i++)
  {
    EEPROM.get(i, value);
    Serial.print("Direccion: ");
    Serial.print(i);
    Serial.print("  Dato almacenado: ");
    Serial.println(value);
  }
  Serial.println("-------------------------- END --------------------------");
  EEPROM.end();
}
